const fs = require("fs");
const Path = require("path");
const express = require("express");
const morgan = require("morgan");
const app = express();

const { filesRouter } = require("./filesRouter.js");

const accessLogStream = fs.createWriteStream(Path.join(__dirname, "access.log"), {
  flags: "a",
});

app.use(express.json());
app.use(morgan("combined", { stream: accessLogStream }));
app.use(morgan("tiny"));

app.use("/api/files", filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync(Path.join(__dirname, "files"))) {
      fs.mkdirSync(Path.join(__dirname, "files"));
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

//ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error("err", err);
  res.status(500).send({ message: "Server error" });
}
