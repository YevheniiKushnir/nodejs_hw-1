const express = require("express");
const router = express.Router();
const { createFile, getFiles, getFile, deleteFile, editFile } = require("./filesService.js");

router.post("/", createFile);

router.get("/", getFiles);

router.get("/:filename", getFile);

// Other endpoints - put, delete.

router.delete("/:filename", deleteFile);

router.put("/", editFile);

module.exports = {
  filesRouter: router,
};
