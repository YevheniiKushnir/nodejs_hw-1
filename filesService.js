// requireturn res...
const fs = require("fs");
const Path = require("path");

// constants...
const private = {};
const extensions = ["txt", "js", "json", "log", "yaml", "xml"];

function createFile(req, res, next) {
  // Your code to create the file.
  try {
    const { filename, content, password } = req.body;
    const path = Path.join(__dirname, "files", filename);

    const ext = filename.match(/([a-zA-Z]{1,})$/m)[0];
    if (!extensions.includes(ext)) throw Error();

    if (fs.existsSync(path)) throw Error();

    if (filename.length === 0 || content.trim().length === 0) throw Error();

    if (password) {
      if (!(password.trim().length > 0)) throw Error();
      private[`${filename}`] = password;
    }

    fs.writeFileSync(path, content);

    return res.status(200).send({ message: "File created successfully" });
  } catch (error) {
    return res.status(400).send({
      message: "Please specify 'content' parameter",
    });
  }
}

function getFiles(req, res, next) {
  // Your code to get all files.
  try {
    const files = fs.readdirSync(Path.join(__dirname, "files"));

    return res.status(200).send({
      message: "Success",
      files,
    });
  } catch (error) {
    return res.status(400).send({
      message: "Client error",
    });
  }
}

const getFile = (req, res, next) => {
  // Your code to get all files.
  const { filename } = req.params;
  const { password } = req.query;

  try {
    if (!filename) throw Error();

    if (!!private[filename] && password !== private[filename]) throw Error();

    const path = Path.join(__dirname, "files", filename);

    const content = fs.readFileSync(path, "utf8");
    const extension = Path.extname(path).split(".")[1];
    const uploadedDate = fs.statSync(path).ctime;

    return res.status(200).send({
      message: "Success",
      filename,
      content,
      extension,
      uploadedDate,
    });
  } catch (error) {
    return res.status(400).send({
      message: "No file with " + filename + " filename found",
    });
  }
};

// Other functions - editFile, deleteFile
function deleteFile(req, res, next) {
  const { filename } = req.params;

  try {
    if (!filename) throw Error();

    const path = Path.join(__dirname, "files", filename);

    fs.unlinkSync(path);

    return res.status(200).send({
      message: "File " + filename + " is delete",
    });
  } catch (error) {
    return res.status(400).send({
      message: "No file with " + filename + " filename found",
    });
  }
}

function editFile(req, res, next) {
  // Your code to edit the file.
  const { filename, content } = req.body;

  try {
    const path = Path.join(__dirname, "files", filename);

    if (filename.length === 0 || content.trim().length === 0) throw Error();

    if (!fs.existsSync(path)) throw Error();

    fs.writeFileSync(path, content);

    return res.status(200).send({ message: "File " + filename + " edited successfully" });
  } catch (error) {
    return res.status(400).send({
      message: "Please specify 'content' parameter",
    });
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile,
  editFile,
};
